Array.from( document.querySelectorAll( 'a' ) ).forEach( a => {
    if(a.protocol == 'gopher:')
    {
        var loc = (a.host + a.pathname).replace(/^\/+/, '');

        a.protocol = 'http:';
        a.host = 'gopher.floodgap.com';
        a.pathname = '/gopher/gw';
        a.search = new URLSearchParams('a=' + loc);
    }
});