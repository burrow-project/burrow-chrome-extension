chrome.runtime.onInstalled.addListener(function (object) {
    if (object.reason === chrome.runtime.OnInstalledReason.INSTALL)
    {
        console.log("Burrow installed with extension ID " + chrome.i18n.getMessage("extension_id"));
        chrome.tabs.create({url: chrome.i18n.getMessage("extension_id") + "/index.html"}, function (tab) {
            console.log("New tab launched with " + chrome.i18n.getMessage("extension_id") + "/index.html");
        });
    }
});